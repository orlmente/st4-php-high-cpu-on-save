# High CPU loads on .php file saves

This repo is meant to demonstrate a bug present in ST4 that could be linked to PHP syntax package.

## The issue
When saving a php file ST4 will spawn a new process that goes at around ~30% (25-35%) CPU load for about ~20 seconds, after that time the new process ends and CPU load turn back to original state.

![Before saving php file](assets/before-saving-php-file.png "Before save")
![While saving php file](assets/while-saving-php-file.png "While saving")
![After saving php file](assets/after-saving-php-file.png "After save")

Actually the .php file doesn't even need to have some real php code in it.

![](assets/while-saving-php-file-without-actual-php.png "While saving without php code")

## Side notes

- Saving any other file extension doesn't trigger the issue, in the repo you'll find html, css and js.
- Changing the syntax to anything else doesn't trigger the issue.
- Whith bigger projects the problem is obviously more noticeable (higher cpu loads and longer duration).
- The issue persists even in a clean install.
- ST3 doesn't seems to have this issue, even though there are some CPU load spikes upon saving the are usually both smaller (around +10%) and faster (~1 second).
