<?php
    $foo = "bar";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" id="main-css-css" href="test.css?v=1.0" type="text/css" media="all">
</head>
<body>
    Test for High CPU on .php file saves

    hitting save spawns a new process that spikes around 30% cpu load for about 20 seconds every single time, after that period the process disappear

    <script type="text/javascript" src="test.js" id="main-js-js"></script>
</body>
</html>